# 12 - Correcting tests and fixing the coverage

The only tests that have failed are where the implementation in our classes has changed.

Meaning it is time to update the out-dated tests.

** Look at the fixed tests **

This was a simple fix that removed even more complexity (which has been delegated to another class).

** Compare the Source class with the original source class **

The size and complexity has not changed that much, but the readability has improved.

** Compare the test class with the original test class **

* The number and size of the tests have reduced (roughly 25% less lines).
* Tests are smaller and easier to read 
* Tests can be written faster (more relaxed,  while still bring in value)
* Less prone to breaking due to implementation changed in other units (methods) or classes.