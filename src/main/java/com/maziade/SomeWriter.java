// Copyright (c) MEI Computer Technology Group Inc, 2008

/**
 * 
 */
package com.maziade;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;

/**
 * @author Eric.Maziade
 *
 */
public class SomeWriter
{
	private final Writer out;

	public SomeWriter(Writer out)
	{
		this.out = out;
	}
	
	public void write(Enum<?> value)
	{
		try
		{
			out.write(value == null ? "null" : value.name());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void write(String value)
	{
		try
		{
			out.write(value);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void write(LocalDateTime value)
	{
		try
		{
			out.write(value.toString());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void write(int value)
	{
		try
		{
			out.write(value);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void writeEndDataBlock()
	{
		try
		{
			out.write("The End");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
