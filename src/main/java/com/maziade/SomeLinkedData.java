// Copyright (c) MEI Computer Technology Group Inc, 2008

/**
 * 
 */
package com.maziade;

import java.math.BigDecimal;

/**
 * @author Eric.Maziade
 *
 */
public class SomeLinkedData
{
	private String name;
	private String code;
	private Long type1;
	private BigDecimal type2;
	private Integer type3;

	public Long getType1()
	{
		return type1;
	}
	public void setType1(Long type1)
	{
		this.type1 = type1;
	}
	public BigDecimal getType2()
	{
		return type2;
	}
	public void setType2(BigDecimal type2)
	{
		this.type2 = type2;
	}
	public Integer getType3()
	{
		return type3;
	}
	public void setType3(Integer type3)
	{
		this.type3 = type3;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
}
