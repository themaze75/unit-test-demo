// Copyright (c) MEI Computer Technology Group Inc, 2008

/**
 * 
 */
package com.maziade;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Eric.Maziade
 *
 */
public class SomeData
{
	private boolean simpleData;
	private boolean pending;
	private LocalDateTime lastUpdated;
	private SomeStatus status;
	private Long type1;
	private BigDecimal type2;
	private Integer type3;
	private List<SomeLinkedData> children;

	public LocalDateTime getLastUpdated()
	{
		return lastUpdated;
	}
	public void setLastUpdated(LocalDateTime lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	public SomeStatus getStatus()
	{
		return status;
	}
	public void setStatus(SomeStatus status)
	{
		this.status = status;
	}
	public Long getType1()
	{
		return type1;
	}
	public void setType1(Long type1)
	{
		this.type1 = type1;
	}
	public BigDecimal getType2()
	{
		return type2;
	}
	public void setType2(BigDecimal type2)
	{
		this.type2 = type2;
	}
	public Integer getType3()
	{
		return type3;
	}
	public void setType3(Integer type3)
	{
		this.type3 = type3;
	}
	public boolean isSimpleData()
	{
		return simpleData;
	}
	public void setSimpleData(boolean simpleData)
	{
		this.simpleData = simpleData;
	}
	public boolean isPending()
	{
		return pending;
	}
	public void setPending(boolean pending)
	{
		this.pending = pending;
	}
	public List<SomeLinkedData> getChildren()
	{
		return children;
	}
	public void setChildren(List<SomeLinkedData> children)
	{
		this.children = children;
	}
	
	
}
