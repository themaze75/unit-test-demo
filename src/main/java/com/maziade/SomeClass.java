// Copyright (c) MEI Computer Technology Group Inc, 2008

/**
 * 
 */
package com.maziade;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Objects;


/**
 * @author Eric.Maziade
 *
 */
public class SomeClass
{
	private static String RES_TITLE_LINKED_DATA = "{{linked}}";
	private static String RES_TITLE_FULL_DATA = "{{full}}";

	private final SomeHelper someHelper;

	//--------------------------------------------------------------------------------------------------------------------------------
	public SomeClass(final SomeHelper someHelper)
	{
		this.someHelper = someHelper;
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	public void addData(SomeWriter output, SomeData data, String title)
	{
		if (data.isSimpleData())
		{
			addSimpleData(output, data, title);
		}
		else
		{
			if (Objects.equals(title, RES_TITLE_LINKED_DATA))
			{
				addLinkedData(output, data, title);
			}
			else if (Objects.equals(title, RES_TITLE_FULL_DATA)) 
			{
				addFullData(output, data, title);
			}
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	protected void addFullData(SomeWriter output, SomeData data, String title)
	{
		output.write("FULL Title");
		output.write(title);
		
		output.write("Last Update");
		output.write(data.getLastUpdated());

		output.write("Status");
		output.write(data.getStatus());

		addChildTypeValue(output, data, SomeEnum.TYPE1, data.getType1());
		addChildTypeValue(output, data, SomeEnum.TYPE2, data.getType2());
		addChildTypeValue(output, data, SomeEnum.TYPE3, data.getType3());

		output.write("Children Count");
		output.write(data.getChildren().size());
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	protected void addLinkedData(SomeWriter output, SomeData data, String title)
	{
		for (SomeLinkedData child : data.getChildren())
		{
			output.write("Child Title");
			output.write(title);
			
			output.write("Name");
			output.write(child.getName());
			output.write("Code");
			output.write(child.getCode());

			addChildTypeValue(output, data, SomeEnum.TYPE1, child.getType1());
			addChildTypeValue(output, data, SomeEnum.TYPE2, child.getType2());
			addChildTypeValue(output, data, SomeEnum.TYPE3, child.getType3());

			output.writeEndDataBlock();
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	protected void addChildTypeValue(SomeWriter output, SomeData data, SomeEnum type, Number typeValue)
	{
		if (someHelper.shouldDisplay(data, type))
		{
			someHelper.writeLabel(output, getLabelForType(type));
			someHelper.writeValue(output, getValueForType(type, typeValue));
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	protected void addSimpleData(SomeWriter output, SomeData data, String title)
	{
		output.write(title);

		if (data.isPending())
		{
			output.write("Data pending");
			output.write(data.getLastUpdated());
		}
		else
		{
			output.write("Status");
			output.write(data.getStatus());
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	protected String getLabelForType(SomeEnum type)
	{
		switch(type)
		{
		case TYPE1:
			return "{{type1}}";
		case TYPE2:
			return "{{type2}}";
		case TYPE3:
			return "{{type3}}";
		default:
			throw new IllegalStateException("Invalid type");
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	protected String getValueForType(SomeEnum type, Number data)
	{
		if (data == null)
			return null;

		switch(type)
		{
			case TYPE1:
				return BigDecimal.valueOf(data.longValue()).toEngineeringString();

			case TYPE2:
				return BigDecimal.valueOf(data.longValue()).toPlainString();

			case TYPE3:
				return NumberFormat.getCurrencyInstance().format(data.doubleValue());

			default:
				throw new IllegalStateException("Invalid type");
		}
	}
}
