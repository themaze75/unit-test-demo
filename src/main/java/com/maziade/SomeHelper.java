// Copyright (c) MEI Computer Technology Group Inc, 2008

/**
 * 
 */
package com.maziade;

import java.time.LocalDateTime;

/**
 * @author Eric.Maziade
 *
 */
public class SomeHelper
{
	public boolean shouldDisplay(SomeData data, SomeEnum dataType)
	{
		if (data.isPending())
			return false;
		
		switch (dataType)
		{
			case TYPE1:
				return data.getLastUpdated().plusDays(15).isBefore(LocalDateTime.now());

			case TYPE2:
				return data.getLastUpdated().plusDays(5).isBefore(LocalDateTime.now());

			case TYPE3:
				return true;

			default:
				return false;
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	public void writeLabel(SomeWriter output, String label)
	{
		output.write("|<<<");
		output.write(label);
		output.write(">>>| ");
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	public void writeValue(SomeWriter output, String value)
	{
		if (value == null)
			output.write("[n/a]");

		else {
			output.write(" = ");
			output.write(value);
		}
	}
}
