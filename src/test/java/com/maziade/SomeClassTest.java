// Copyright (c) MEI Computer Technology Group Inc, 2008

package com.maziade;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

public class SomeClassTest
{

	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addData_simpleData()
	{
		final SomeClass spiedInstance = spy(new SomeClass(null));
		
		final SomeWriter output = null;
		final SomeData data = new SomeData();
		final String title = "RandomTitle";

		data.setSimpleData(true);

		doNothing().when(spiedInstance).addSimpleData(any(), any(),  any());

		//-------------------------------
		spiedInstance.addData(output, data, title);

		verify(spiedInstance).addData(output, data, title);
		verify(spiedInstance).addSimpleData(output, data, title);
		verifyNoMoreInteractions(spiedInstance);
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addData_linkedData()
	{
		final SomeClass spiedInstance = spy(new SomeClass(null));
		
		final SomeWriter output = null;
		final SomeData data = new SomeData();
		final String title = "{{linked}}";

		doNothing().when(spiedInstance).addLinkedData(any(), any(),  any());

		//-------------------------------
		spiedInstance.addData(output, data, title);

		verify(spiedInstance).addData(output, data, title);
		verify(spiedInstance).addLinkedData(output, data, title);
		verifyNoMoreInteractions(spiedInstance);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addData_addFullData()
	{
		final SomeClass spiedInstance = spy(new SomeClass(null));
		
		final SomeWriter output = null;
		final SomeData data = new SomeData();
		final String title = "{{full}}";

		doNothing().when(spiedInstance).addFullData(any(), any(),  any());

		//-------------------------------
		spiedInstance.addData(output, data, title);

		verify(spiedInstance).addData(output, data, title);
		verify(spiedInstance).addFullData(output, data, title);
		verifyNoMoreInteractions(spiedInstance);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addData_addOtherData()
	{
		final SomeClass spiedInstance = spy(new SomeClass(null));
		
		final SomeWriter output = null;
		final SomeData data = new SomeData();
		final String title = "{{somethingElse}}";

		//-------------------------------
		spiedInstance.addData(output, data, title);

		verify(spiedInstance).addData(output, data, title);
		verifyNoMoreInteractions(spiedInstance);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addSimpleData_withPending()
	{
		final SomeClass instance = new SomeClass(null);
		
		final SomeWriter output = mock(SomeWriter.class);
		final SomeData data = new SomeData();

		final String title = "RandomTitle12134"; 
		final LocalDateTime lastUpdated = LocalDateTime.now().minusYears(2);

		data.setPending(true);
		data.setLastUpdated(lastUpdated);

		//-------------------------------
		instance.addSimpleData(output, data, title);

		final InOrder inOrder = Mockito.inOrder(output);
		
		inOrder.verify(output).write(title);
		inOrder.verify(output).write("Data pending");
		inOrder.verify(output).write(lastUpdated);
		
		verifyNoMoreInteractions(output);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addSimpleData_withoutPending()
	{
		final SomeClass instance = new SomeClass(null);
		
		final SomeWriter output = mock(SomeWriter.class);
		final SomeData data = new SomeData();

		final String title = "RandomTitle12134"; 
		final SomeStatus status = SomeStatus.STATUS1;

		data.setPending(false);
		data.setStatus(status);

		//-------------------------------
		instance.addSimpleData(output, data, title);

		final InOrder inOrder = Mockito.inOrder(output);

		inOrder.verify(output).write(title);
		inOrder.verify(output).write("Status");
		inOrder.verify(output).write(status);

		verifyNoMoreInteractions(output);
	}
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addLinkedData()
	{
		// TODO There is no more branching in this method, so only one test is required for full coverage of the method
		final SomeClass spiedInstance = spy(new SomeClass(null));
		
		final SomeWriter output = mock(SomeWriter.class);
		final SomeData data = new SomeData();
		final String title = "SomeasdasdRandomTitle1234adwq";

		final SomeLinkedData link1 = new SomeLinkedData();
		final String name1 = "name1";
		link1.setName(name1);
		final String code1 = "code1";
		link1.setCode(code1);
		final Long type1Value1 = Long.valueOf(1_234);
		link1.setType1(type1Value1);
		final BigDecimal type2Value1 = BigDecimal.valueOf(1_234_567);
		link1.setType2(type2Value1);
		final Integer type3Value1 = Integer.valueOf(123);
		link1.setType3(type3Value1);

		final SomeLinkedData link2 = new SomeLinkedData();
		final String name2 = "name2";
		link2.setName(name2);
		final String code2 = "code2";
		link2.setCode(code2);
		final Long type1Value2 = Long.valueOf(2_234);
		link2.setType1(type1Value2);
		final BigDecimal type2Value2 = BigDecimal.valueOf(2_234_567);
		link2.setType2(type2Value2);
		final Integer type3Value2 = Integer.valueOf(223);
		link2.setType3(type3Value2);

		data.setChildren(Arrays.asList(link1, link2));
		
		doNothing().when(spiedInstance).addChildTypeValue(any(), any(), any(), any());

		//-------------------------------
		spiedInstance.addLinkedData(output, data, title);

		final InOrder inOrder = Mockito.inOrder(output, spiedInstance);
		
		verify(spiedInstance).addLinkedData(output, data, title);

		inOrder.verify(output).write("Child Title");
		inOrder.verify(output).write(title);
		inOrder.verify(output).write("Name");
		inOrder.verify(output).write(name1);
		inOrder.verify(output).write("Code");
		inOrder.verify(output).write(code1);
		
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE1, type1Value1);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE2, type2Value1);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE3, type3Value1);

		inOrder.verify(output).writeEndDataBlock();

		inOrder.verify(output).write("Child Title");
		inOrder.verify(output).write(title);
		inOrder.verify(output).write("Name");
		inOrder.verify(output).write(name2);
		inOrder.verify(output).write("Code");
		inOrder.verify(output).write(code2);

		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE1, type1Value2);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE2, type2Value2);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE3, type3Value2);

		inOrder.verify(output).writeEndDataBlock();

		verifyNoMoreInteractions(output, spiedInstance);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addFullData()
	{
		// TODO There is no more branching in this method, so only one test is required for full coverage of the method
		final SomeClass spiedInstance = spy(new SomeClass(null));

		final SomeWriter output = mock(SomeWriter.class);
		final SomeData data = new SomeData();
		final String title = "SomeRandomTitle1234";

		final SomeStatus status = SomeStatus.STATUS3;
		data.setStatus(status);

		final LocalDateTime lastUpdated = LocalDateTime.now().minusDays(10);
		data.setLastUpdated(lastUpdated);

		final Long valueType1 = Long.valueOf(1_234);
		final BigDecimal valueType2 = BigDecimal.valueOf(1_234_567);
		final Integer valueType3 = Integer.valueOf(123);

		data.setType1(valueType1);
		data.setType2(valueType2);
		data.setType3(valueType3);

		// 3 children
		data.setChildren(Arrays.asList(new SomeLinkedData(), new SomeLinkedData(), new SomeLinkedData()));

		doNothing().when(spiedInstance).addChildTypeValue(any(), any(), any(), any());

		//-------------------------------
		spiedInstance.addFullData(output, data, title);

		final InOrder inOrder = Mockito.inOrder(output, spiedInstance);

		inOrder.verify(output).write("FULL Title");
		inOrder.verify(output).write(title);
		inOrder.verify(output).write("Last Update");
		inOrder.verify(output).write(lastUpdated);
		inOrder.verify(output).write("Status");
		inOrder.verify(output).write(status);

		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE1, valueType1);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE2, valueType2);
		inOrder.verify(spiedInstance).addChildTypeValue(output, data, SomeEnum.TYPE3, valueType3);

		inOrder.verify(output).write("Children Count");
		inOrder.verify(output).write(3);

		verifyNoMoreInteractions(output);
	}

	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addChildTypeValue_withShouldDisplay()
	{
		final SomeHelper someHelper = mock(SomeHelper.class);

		final SomeClass spiedInstance = spy(new SomeClass(someHelper));
		
		final SomeWriter output = mock(SomeWriter.class);
		final SomeData data = new SomeData();
		final SomeEnum type = SomeEnum.TYPE2;
		final Number typeValue = Integer.valueOf(123_123_344);

		doReturn(Boolean.TRUE).when(someHelper).shouldDisplay(data, type);
		doNothing().when(someHelper).writeLabel(any(), any());
		doNothing().when(someHelper).writeValue(any(), any());

		final String label = "{{{label}}}";
		doReturn(label).when(spiedInstance).getLabelForType(type);

		final String value = "{{{value}}}";
		doReturn(value).when(spiedInstance).getValueForType(type, typeValue);

		//--------------------------
		spiedInstance.addChildTypeValue(output, data, type, typeValue);

		verify(someHelper).shouldDisplay(data, type);

		final InOrder inOrder = inOrder(output, someHelper);

		inOrder.verify(someHelper).writeLabel(output, label);
		inOrder.verify(someHelper).writeValue(output, value);

		// No more output for this method
		verifyNoMoreInteractions(output, someHelper);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_addChildTypeValue_withoutShouldDisplay()
	{
		final SomeHelper someHelper = mock(SomeHelper.class);

		final SomeClass spiedInstance = spy(new SomeClass(someHelper));
		
		final SomeWriter output = null;
		final SomeData data = new SomeData();
		final SomeEnum type = SomeEnum.TYPE2;
		final Number typeValue = Integer.valueOf(123_123_344);

		doReturn(Boolean.FALSE).when(someHelper).shouldDisplay(data, type);

		//--------------------------
		spiedInstance.addChildTypeValue(output, data, type, typeValue);

		// Redundant validation for Sonar 
		verify(someHelper).shouldDisplay(data, type);
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_getLabelForType()
	{
		final SomeClass instance = new SomeClass(null);

		//--------------------------
		assertEquals("{{type1}}", instance.getLabelForType(SomeEnum.TYPE1));

		//--------------------------
		assertEquals("{{type2}}", instance.getLabelForType(SomeEnum.TYPE2));

		//--------------------------
		assertEquals("{{type3}}", instance.getLabelForType(SomeEnum.TYPE3));
		
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------
	@Test
	public void test_getValueForType()
	{
		final SomeClass instance = new SomeClass(null);

		//--------------------------
		assertNull(instance.getValueForType(null, null));

		//--------------------------
		final Long data1 = Long.valueOf(4_567_8910);
		assertEquals(BigDecimal.valueOf(data1.longValue()).toEngineeringString(), instance.getValueForType(SomeEnum.TYPE1, data1));

		//--------------------------
		final BigDecimal data2 = BigDecimal.valueOf(5_678);
		assertEquals(BigDecimal.valueOf(data2.longValue()).toPlainString(), instance.getValueForType(SomeEnum.TYPE2, data2));

		//--------------------------
		final BigDecimal data3 = BigDecimal.valueOf(5_678);
		assertEquals(NumberFormat.getCurrencyInstance().format(data3.doubleValue()), instance.getValueForType(SomeEnum.TYPE3, data3));
	}
}

