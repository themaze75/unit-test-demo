# Unit Test Scenario

You just joined an organization that has implemented a unit testing strategy aiming for:

- 100 % coverage
- using the class method as the unit

This exercise aims to explore the concepts of:

* reaching maximum coverage
* using the method as a unit 
* demonstrate how reducing code complexity can
  * write more readable code 
  * improve on test coverage 
  * write simpler tests 
  * facilitate the maintenance of the tests following refactoring and changes of specification

## How to use ?

- Look at all the commits one by one
- Start by looking at the `CHAPTER.md` file for context.
- Look at the `TODO` markers in the code

Using git within Eclipse, I find it useful to start an interactive rebase on the "00-START" commit, marking all commits as editable.

It allows me to move forward in the presentation by simpliy following the rebase process.

Navigate the commits and look at the comments and todo markers in the code.